#include <windows.h>
#include <GL\glew.h>
#include <SDL2\SDL.h>
#include <cstdio>


int main(int argc, char** argv) {
	//Init GLFW
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		SDL_Log("Couldn't initialize SDL: %s", SDL_GetError());
		SDL_Quit();
		return -1;
	}
	// Create window
	SDL_Window *mainwindow;
	SDL_GLContext maincontext;

	SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

	mainwindow = SDL_CreateWindow("GL_framework", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
	if (mainwindow == NULL) { /* Die if creation failed */
		SDL_Log("Couldn't create SDL window: %s", SDL_GetError());
		SDL_Quit();
		return -1;
	}

	/* Create our opengl context and attach it to our window */
	maincontext = SDL_GL_CreateContext(mainwindow);
	if (maincontext == NULL) {
		SDL_Log("Couldn't create SDL GL context: %s", SDL_GetError());
		SDL_Quit();
		return -1;
	}

	// Init GLEW
	GLenum err = glewInit();
	if(GLEW_OK != err) {
		SDL_Log("Glew error: %s\n", glewGetErrorString(err));
	}
	SDL_Log("Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));

	bool quit_app = false;
	while (!quit_app) {
		SDL_Event eve;
		while (SDL_PollEvent(&eve)) {
			switch (eve.type) {
			case SDL_QUIT:
				quit_app = true;
				break;
			}
		}
	}
	
	SDL_GL_DeleteContext(maincontext);
	SDL_DestroyWindow(mainwindow);
	SDL_Quit();
	return 0;
}
